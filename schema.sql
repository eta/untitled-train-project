CREATE TABLE nodes (
       osm_id BIGINT PRIMARY KEY,
       easting REAL NOT NULL,
       northing REAL NOT NULL,
       flags INT NOT NULL DEFAULT 0,
       component BIGINT NOT NULL DEFAULT -1
);

CREATE INDEX nodes_flags ON nodes (flags);
CREATE INDEX nodes_easting ON nodes (easting);
CREATE INDEX nodes_northing ON nodes (northing);


CREATE TABLE links (
       p1 INT NOT NULL REFERENCES nodes,
       p2 INT NOT NULL REFERENCES nodes,
       flags INT NOT NULL DEFAULT 0,
       parent_way_id INT,
       UNIQUE(p1, p2, flags, parent_way_id)
);

CREATE INDEX links_p1 ON links (p1);
CREATE INDEX links_p2 ON links (p2);
CREATE INDEX links_flags ON links (flags);
CREATE INDEX links_parent_way_id ON links (parent_way_id);

CREATE TABLE stations (
       id SERIAL PRIMARY KEY,
       crs TEXT,
       tiploc TEXT,
       osm_id BIGINT,
       is_way BOOL NOT NULL,
       name TEXT
);

CREATE INDEX stations_crs ON stations (crs);
CREATE INDEX stations_tiploc ON stations (tiploc);

CREATE TABLE astations (
       id INTEGER PRIMARY KEY,
       node_members TEXT NOT NULL,
       easting REAL NOT NULL,
       northing REAL NOT NULL,
       UNIQUE(easting, northing)
);

CREATE INDEX astations_node_members ON astations (node_members);

CREATE TABLE astations_tiploc (
       station_id INT NOT NULL REFERENCES astations,
       tiploc TEXT UNIQUE NOT NULL
);
