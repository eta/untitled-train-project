# Projections Setup Guide

- Install PROJ 6
- Run `$ projinfo -s EPSG:4326 -t EPSG:27700`. The following error message should appea: `Grid OSTN15_NTv2_OSGBtoETRS.gsb needed but not found on the system. Can be obtained from the proj-datumgrid-europe package at https://download.osgeo.org/proj/proj-datumgrid-europe-1.5.zip`.
- To fix this:
  - Download [this zip](https://www.ordnancesurvey.co.uk/documents/resources/OSTN15-NTv2.zip)
  - Extract `OSTN15_NTv2_OSGBtoETRS.gsb` and place it in `/usr/share/proj`
  - Rerun the above command; error should have vanished
