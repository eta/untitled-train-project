;;;; Slurping up railway-related data from OSM

(in-package :railway-slurper)

(setf osmpbf:*tag-value-translation-names* '(:railway :building))

(defvar *db* nil
  "Connection to the database.")
(defparameter *default-database-path* "data.sqlite3"
  "Default path to the SQLite database file.")
(defvar *prepared-statements* nil
  "List of statements prepared by PREPARED-STATEMENT.")
(defparameter *sqlite-pragmas*
  '("PRAGMA journal_mode = WAL"
    "PRAGMA wal_autocheckpoint = 5000"
    "PRAGMA synchronous = NORMAL")
  "List of SQLite pragmas to run on connection to make things bearable")
(defvar *nodes-inserted* 0
  "Number of nodes inserted.")
(defvar *ways-inserted* 0
  "Number of ways inserted.")
(defvar *stations-inserted* 0
  "Number of stations inserted.")
(defvar *wgs84-to-grid-obj* nil
  "A PROJ conversion object for doing WGS84 to National Grid easting/northing conversion.")
(defvar *troublesome-tiplocs* '()
  "A list of TIPLOCs where stations couldn't be made.")

(defun run-pragmas ()
  "Runs all statements in *SQLITE-PRAGMAS*."
  (mapc (lambda (x) (sqlite:execute-non-query *db* x)) *sqlite-pragmas*))

(defun connect-database (&optional (path *default-database-path*))
  "Establish a connection to the database."
  (setf *db* (sqlite:connect path))
  (run-pragmas)
  (loop for sym in *prepared-statements*
        do (eval `(setf ,sym nil)))
  (setf *prepared-statements* nil))

(defmacro prepared-statement (statement)
  "Caches the creation of a prepared statement with SQL text STATEMENT.
In other words, prepares STATEMENT once, then returns the prepared statement after that instead of doing that work again."
  (let ((statement-sym (gensym "PREPARED-STATEMENT-")))
    (eval `(defvar ,statement-sym nil))
    `(progn
       (defvar ,statement-sym nil)
       (unless ,statement-sym
         (setf ,statement-sym (sqlite:prepare-statement *db* ,statement))
         (setf *prepared-statements* (cons ',statement-sym *prepared-statements*)))
       ,statement-sym)))

(defmacro with-prepared-statement ((name statement) &body forms)
  "Evaluates FORMS, binding a prepared statement with SQL text STATEMENT to NAME, and ensuring it is reset when control is transferred."
  `(let ((,name (prepared-statement ,statement)))
     (unwind-protect
          (progn ,@forms)
       (ignore-errors (sqlite:reset-statement ,name)))))

(defmacro with-prepared-statements (statements &body forms)
  "Like WITH-PREPARED-STATEMENT, but takes multiple statements."
  (let ((let-forms (loop for (name statement) in statements
                         collect `(,name (prepared-statement ,statement))))
        (reset-forms (loop for (name statement) in statements
                           collect `(ignore-errors (sqlite:reset-statement ,name)))))
    `(let (,@let-forms)
       (unwind-protect
            (progn ,@forms)
         (ignore-errors (progn ,@reset-forms))))))

(defmacro bind-parameters (statement &rest parameters)
  "Binds PARAMETERS to the prepared statement STATEMENT.

PARAMETERS are either simple values (in which case they're bound to parameters 1, 2, ...),
or cons cells, where the `car` is the index to bind to and the `cdr' is the value to use."
  `(progn
     ,@(loop for param in parameters
             for idx from 1 upto (length parameters)
             collect (if (listp param)
                         `(sqlite:bind-parameter ,statement ,(car param) ,(second param))
                         `(sqlite:bind-parameter ,statement ,idx ,param)))))

(defvar +level-crossing-tag+ #b1)
(defvar +rail-tag+ #b10)
(defvar +station-tag+ #b100)
(defvar +platform-tag+ #b1000)
(defvar +station-building-tag+ #b10000)
(defvar +station-node-tag+ #b100000)
(defvar +station-link-tag+ #b1000000)
(defvar +navigation-tags+ (logior +station-link-tag+ +rail-tag+))

(defun get-flag-for-tag-cons (tag)
  "Get the relevant integer flag for the OSM tag cons cell TAG."
  (if (not (eql (car tag) :railway))
      (if (and (eql (car tag) :building) (eql (cdr tag) :train-station))
          +station-building-tag+
          0)
      (case (cdr tag)
        (:crossing +level-crossing-tag+)
        (:level-crossing +level-crossing-tag+)
        (:rail +rail-tag+)
        (:station +station-tag+)
        (:platform +platform-tag+)
        (t 0))))

(defun get-flags-for-tags (tags)
  "Get an integer 'flags' value for the TAGS on a node."
  (reduce #'logior (mapcar #'get-flag-for-tag-cons tags)))

(defun contains-flag-p (flags flag)
  "Returns T if FLAG is in FLAGS."
  (eql (logand flags flag) flag))

(defun sqlite-boolean (value)
  "Converts VALUE to a SQLite BOOL type."
  (if value 1 0))

(defmacro with-bound-columns (parameters statement &body forms)
  "Binds each column value of STATEMENT to the symbols in PARAMETERS, and runs FORMS."
  (let ((let-forms (loop
                     for param in parameters
                     for idx from 0 upto (1- (length parameters))
                     collect `(,param (sqlite:statement-column-value ,statement ,idx)))))
    `(let (,@let-forms) ,@forms)))

(defun node-contains-flag-p (nid flag)
  "Returns T if FLAG is in the node with ID NID's flags."
  (with-prepared-statements
      ((stmt "SELECT flags FROM nodes WHERE osm_id = ?"))
    (bind-parameters stmt nid)
    (assert (sqlite:step-statement stmt) () "invalid node id ~A" nid)
    (with-bound-columns (flags) stmt
      (contains-flag-p flags flag))))

(defun adjacent-nodes (node-id &optional flags avoid-node)
  (with-prepared-statements
      ((stmt "SELECT p1, p2 FROM links WHERE (p1 = ?1 OR p2 = ?1) AND (flags | ?2) > 0"))
    (bind-parameters stmt node-id flags)
    (loop
      while (sqlite:step-statement stmt)
      with other
      do (with-bound-columns (p1 p2) stmt
           (setf other (if (eql p1 node-id) p2 p1)))
      when (not (eql other avoid-node))
        collect other)))

(defmacro each-adjacent-node (other-id (node &optional flags strictp) &body forms)
  "Run FORMS repeatedly, binding OTHER-ID to the node ID of each node connected to NODE by a link with FLAGS set.
If STRICTP is T, treats the graph as directed.
If FLAGS is NIL, the links used are not restricted."
  (alexandria:with-gensyms (stmt lax-stmt strict-stmt p1 p2)
    `(with-prepared-statements
         ((,lax-stmt "SELECT p1, p2 FROM links WHERE (p1 = ?1 OR p2 = ?1) AND (flags | ?2) > 0")
          (,strict-stmt "SELECT p1, p2 FROM links WHERE p1 = ?1 AND (flags | ?2) > 0"))
       (let ((,stmt (if ,strictp ,strict-stmt ,lax-stmt)))
         (loop
           initially (bind-parameters ,stmt ,node ,flags)
           while (sqlite:step-statement ,stmt)
           do (with-bound-columns (,p1 ,p2) ,stmt
                (let ((,other-id (if (eql ,p1 ,node) ,p2 ,p1)))
                  ,@forms)))))))

(defun node-component (nid)
  "Returns the component of the node with id NID."
  (with-prepared-statement
      (stmt "SELECT component FROM nodes WHERE osm_id = ?")
    (bind-parameters stmt nid)
    (unless (sqlite:step-statement stmt)
      (error "No node with ID ~A exists." nid))
    (with-bound-columns (ret) stmt ret)))

(defun components-equal-p (n1 n2)
  "Returns T if N1 and N2 share a component, and NIL otherwise."
  (eql (node-component n1) (node-component n2)))

(defun node-location (nid)
  "Returns the easting and northing of the node with id NID, as two values."
  (with-prepared-statement
      (stmt "SELECT easting, northing FROM nodes WHERE osm_id = ?")
      (bind-parameters stmt nid)
    (unless (sqlite:step-statement stmt)
      (error "No node with ID ~A exists." nid))
    (with-bound-columns (easting northing) stmt
      (values easting northing))))

(defun squared-pythagoras-distance (x1 y1 x2 y2)
  (declare (double-float x1 y1 x2 y2) (optimize (speed 3)))
  (+
   (expt (- x2 x1) 2)
   (expt (- y2 y1) 2)))

(defun pythagoras-distance (x1 y1 x2 y2)
  (declare (double-float x1 y1 x2 y2) (optimize (speed 3)))
  (the double-float (sqrt (squared-pythagoras-distance x1 y1 x2 y2))))

(defun distance-between-nodes (n1 n2)
  "Returns the distance, in meters, between the nodes N1 and N2."
  (multiple-value-bind (x1 y1) (node-location n1)
    (multiple-value-bind (x2 y2) (node-location n2)
      (pythagoras-distance x1 y1 x2 y2))))

(defun find-link-heading (n1 n2)
  "Finds the heading between nodes N1 and N2."
  (multiple-value-bind (x1 y1) (node-location n1)
    (multiple-value-bind (x2 y2) (node-location n2)
      (find-heading x1 y1 x2 y2))))

(defun route-from-prev (end prev)
  "Given a previous-hop array PREV, and supplied START and END nodes, return the path found from START to END."
  (nreverse
   (cons end (loop with node = end
                   while (setf node (gethash node prev))
                   collect node))))

(defparameter +gpx-header+
  "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
   <gpx version=\"1.0\">
   <name>Routing output</name>
   <trk><name>Route</name><number>1</number><trkseg>")
(defparameter +gpx-footer+
  "</trkseg></trk></gpx>")

(defun crude-gpx (list stream)
  "Prints out a crudely constructed GPX file from the route between the node ids given in LIST."
  (princ +gpx-header+ stream)
  (terpri stream)
  (loop
    for node in list
    do (multiple-value-bind (easting northing) (node-location node)
         (multiple-value-bind (lat lon)
             (proj-translate *wgs84-to-grid-obj* easting northing 0.0d0 +pj-inv+)
           (format stream "<trkpt lat=\"~F\" lon=\"~F\" />~%"
                   lat lon))))
  (princ +gpx-footer+ stream)
  (terpri stream))

(defun route-between-tiplocs (start end)
  (let* ((start-aid (get-astation-for-tiploc start))
         (end-aid (get-astation-for-tiploc end))
         (start-node (first (get-astation-node-members start-aid)))
         (end-node (first (get-astation-node-members end-aid))))
    (unless (and start-node end-node)
      (error "Couldn't find nodes for ~A -> ~A." start end))
    (dijkstra start-node end-node)))

(defun dijkstra (start end)
  "Route between nodes with ids START and END using Dijkstra's shortest-path algorithm.
START and END must be in the same component, otherwise an error will be signaled."
  (unless (components-equal-p start end)
    (error "Nodes ~A and ~A aren't in the same component." start end))
  (let ((distances (make-hash-table))
        (prev (make-hash-table))
        (queue (make-instance 'cl-heap:priority-queue))
        (considered 0)
        (updated 0))
    (setf (gethash start distances) 0)
    (cl-heap:enqueue queue start 0.0d0)
    (loop
      with node = nil
      while (setf node (cl-heap:dequeue queue))
      do (each-adjacent-node other (node +rail-tag+)
           (when (eql (rem considered 1000) 0)
             (format *debug-io* "; considering ~A (~A/~A)~%" other considered updated))
           (incf considered)
           (symbol-macrolet
               ((node-dist (gethash node distances))
                (other-dist (gethash other distances)))
             (let ((alt-dist (+ node-dist
                                (distance-between-nodes node other))))
               (when (or (null other-dist) (< alt-dist other-dist))
                 (incf updated)
                 (setf other-dist alt-dist)
                 (setf (gethash other prev) node)
                 (cl-heap:enqueue queue other alt-dist))
               (when (eql other end)
                 (format *debug-io* ";; Found a path after ~Ac/~Au!~%" considered updated)
                 (return-from dijkstra (route-from-prev end prev))))))
      finally (error "Route not found!"))))

(defvar *dfs-stack* (make-array 64
                                :element-type 'fixnum
                                :fill-pointer 0
                                :initial-element 0)
  "Reusable stack allocation for DFS-ITERATIVE calls.")

(defun dfs-iterative (start flags)
  "Iterative depth-first search that isn't actually searching for anything, starting at START."
  (let ((stack *dfs-stack*)
        (visited (make-hash-table)))
    (setf (fill-pointer stack) 0)
    (vector-push start stack)
    (loop
      while (not (eql (length stack) 0))
      do (let ((vertex (vector-pop stack)))
           (when (not (gethash vertex visited))
             (setf (gethash vertex visited) t)
             (each-adjacent-node other (vertex flags)
               (vector-push-extend other stack)))))
    visited))

(defun mark-node-component (node component)
  "Sets the component id of the node with id NODE to COMPONENT."
  (with-prepared-statement
      (stmt "UPDATE nodes SET component = ? WHERE osm_id = ?")
    (bind-parameters stmt component node)
    (sqlite:step-statement stmt)))

(defun mark-component-starting-at (start flags)
  "Runs DFS-ITERATIVE starting from START, and then marks all visited nodes as belonging to a new component.
Returns the number of nodes in the new component, and the component ID as the second value (which is currently just START)."
  (let ((visited (dfs-iterative start flags)))
    (loop
      for node being the hash-keys in visited
      do (mark-node-component node start))
    (values (hash-table-count visited) start)))

(defun mark-one-node-islands (flags)
  "As an optimization, finds nodes that aren't connected to anything else and puts them in their own components.
This avoids running the DFS for these stupid cases where it probably takes longer to do the function call than actually search...
Returns the number of islands marked."
  (with-prepared-statement
      (stmt "SELECT osm_id FROM nodes WHERE component = -1 AND NOT EXISTS(SELECT * FROM links WHERE ((links.p1 = nodes.osm_id) OR (links.p2 = nodes.osm_id)) AND (flags | ?1) > 0)")
    (sqlite:with-transaction *db*
      (loop
        initially (bind-parameters stmt flags)
        while (sqlite:step-statement stmt)
        count t
        do (with-bound-columns (node) stmt
             (mark-node-component node node))))))

(defun mark-components (flags)
  "Sorts all unsorted nodes in the database into graph components, updating the database to match."
  (with-prepared-statement
      (stmt "SELECT osm_id FROM nodes WHERE component = -1 LIMIT 1")
    (format *debug-io* ";; Marking one-node islands~%")
    (let ((onis (mark-one-node-islands flags)))
      (format *debug-io* ";; Marked ~A one-node islands~%" onis))
    (sqlite:with-transaction *db*
      (loop
        while (sqlite:step-statement stmt)
        do (with-bound-columns (node) stmt
             (let ((num-nodes (mark-component-starting-at node flags)))
               (format *debug-io* ";; Component with node ~A has ~A nodes~%" node num-nodes))
             (sqlite:reset-statement stmt))))
    t))

(defun insert-osm-station (node-id way-id tags)
  "Insert an OSM station, with either a NODE-ID or a WAY-ID, and the given TAGS."
  (with-prepared-statement
      (station-stmt "INSERT INTO stations (crs, tiploc, osm_id, is_way, name) VALUES (?, ?, ?, ?, ?)")
    (let ((osm-id (or node-id way-id))
          (is-way (sqlite-boolean (null node-id)))
          (crs (cdr (assoc :ref/crs tags)))
          (tiploc (cdr (assoc :ref/tiploc tags)))
          (name (cdr (assoc :name tags))))
      (bind-parameters station-stmt crs tiploc osm-id is-way name)
      (sqlite:step-statement station-stmt)
      (incf *stations-inserted*))))

(defun insert-osm-node (node)
  "Inserts the OSM node NODE into the database."
  (with-prepared-statement
      (stmt "INSERT INTO nodes (osm_id, easting, northing, flags) VALUES (?, ?, ?, ?)")
    (with-accessors ((osm-id osmpbf:osm-obj-id) (tags osmpbf:osm-obj-tags) (lat osmpbf:node-lat) (lon osmpbf:node-lon)) node
      (let ((flags (get-flags-for-tags tags)))
        (multiple-value-bind (easting northing)
            (proj-translate *wgs84-to-grid-obj* lat lon 0.0d0)
          (bind-parameters stmt osm-id easting northing flags)
          (sqlite:step-statement stmt)
          (incf *nodes-inserted*)
          (when (contains-flag-p flags +station-tag+)
            (insert-osm-station osm-id nil tags)))))))

(defun insert-osm-way-links (refs flags parent-way-id)
  "Inserts links between OSM nodes specified in REFS, using the given FLAGS and PARENT-WAY-ID."
  (with-prepared-statement
      (stmt "INSERT INTO links (p1, p2, flags, parent_way_id) VALUES (?, ?, ?, ?)")
    (loop for i from 0 upto (- (length refs) 2)
          do (progn
               (handler-case
                   (let ((p1 (min (aref refs i) (aref refs (1+ i))))
                         (p2 (max (aref refs i) (aref refs (1+ i)))))
                     (bind-parameters stmt p1 p2 flags parent-way-id)
                     (sqlite:step-statement stmt))
                 (sqlite:sqlite-constraint-error (c)
                   (warn "Inserting link ~A<->~A failed: ~A"
                         (aref refs i) (aref refs (1+ i)) c)))
               (ignore-errors (sqlite:reset-statement stmt))))))

(defun line-project-point (px py x1 y1 x2 y2)
  "Finds the point on the line segment (x1, y1) -> (x2, y2) closest to the point (px, py), or NIL
if the perpendicular to the line going through the point does not intersect the line."
  (declare (double-float px py x1 y1 x2 y2) (optimize (speed 3)))
  (let* ((line-dx (- x2 x1))
         (line-dy (- y2 y1))
         (point-dx (- px x1))
         (point-dy (- py y1))
         (dot-product (+ (* line-dx point-dx) (* line-dy point-dy)))
         (line-length-squared (+ (expt line-dx 2) (expt line-dy 2)))
         (projection-scalar (if (> line-length-squared 0.0d0)
                                (/ dot-product line-length-squared)
                                -1.0d0)))
    (when (>= 1.0d0 projection-scalar 0.0d0)
      (values
       (+ x1 (* projection-scalar line-dx))
       (+ y1 (* projection-scalar line-dy))))))

(defun find-heading (x1 y1 x2 y2)
  "Finds the heading of the line segment A(x1, y1) -> B(x2, y2) -- the argument of B if A is the origin, in whole degrees."
  (declare (double-float x1 y1 x2 y2) (optimize (speed 3)))
  (let* ((dx (- x2 x1))
         (dy (- y2 y1))
         (arg-radians (atan dy dx))
         (arg-degrees (/ (* arg-radians 360) (* 2 pi))))
    arg-degrees))

(defun largest-osm-id ()
  "Return the largest osm_id value in the database."
  (with-prepared-statement
      (stmt "SELECT osm_id FROM nodes ORDER BY osm_id DESC LIMIT 1")
      (assert (sqlite:step-statement stmt))
    (with-bound-columns (id) stmt
      id)))

(defun make-special-node (n1 n2 px py flags)
  "Insert a node at (px, py) between the link n1 <-> n2 with the specified flags. Returns the node ID of the new node."
  (with-prepared-statements
      ((get-flags-stmt "SELECT flags FROM links WHERE p1 = ? AND p2 = ?")
       (node-stmt "INSERT INTO nodes (osm_id, easting, northing, flags) VALUES (?, ?, ?, ?)")
       (link-stmt "INSERT INTO links (p1, p2, flags) VALUES (?, ?, ?)"))
    (bind-parameters get-flags-stmt n1 n2)
    (assert (sqlite:step-statement get-flags-stmt) ()
            "MAKE-SPECIAL-NODE called with invalid link ~A <-> ~A" n1 n2)
    (with-bound-columns (link-flags) get-flags-stmt
      (let ((new-id (1+ (largest-osm-id))))
        (bind-parameters node-stmt new-id px py flags)
        (sqlite:step-statement node-stmt)
        (bind-parameters link-stmt n1 new-id link-flags)
        (sqlite:step-statement link-stmt)
        (sqlite:reset-statement link-stmt)
        (bind-parameters link-stmt new-id n2 link-flags)
        (sqlite:step-statement link-stmt)
        new-id))))

(defun identify-station (px py)
  "Run the station identification algorithm, starting at the point (px, py). Returns a vector of station points created."
  (flet ((make-node-vector ()
           (make-array 16
                       :fill-pointer 0
                       :element-type 'fixnum
                       :adjustable t)))
    (let ((nodes-to-exclude (make-node-vector))
          (station-nodes (make-node-vector))
          (old-heading)
          (px px)
          (py py))
      (loop
        (multiple-value-bind (dist n1 n2 perp-x perp-y)
            (get-closest-link px py
                              :exclude-nodes nodes-to-exclude)
          (unless (and dist (or (not old-heading) (< dist 40.0d0)))
            (format *debug-io* ";; Unable to find track close by, aborting (dist: ~,2Fm)~%" dist)
            (return))
          (let* ((link-heading (abs (find-link-heading n1 n2))))
            (when old-heading
              (let ((heading-diff (min
                                   (abs (- link-heading old-heading))
                                   (abs (- link-heading (- 180 old-heading))))))
                (when (> heading-diff 30.0d0)
                  (format *debug-io* ";; Heading difference of ~,2F° (new ~,2F°; old ~,2F°) is too great, aborting~%" heading-diff link-heading old-heading)
                  (return))))
            (setf old-heading link-heading))
          (let ((new-node-id
                  (make-special-node n1 n2 perp-x perp-y +station-node-tag+)))
            (format *debug-io* "; Made new node ~A at (~,2F, ~,2F) - track hdg ~,2F°~%" new-node-id perp-x perp-y old-heading)
            (vector-push-extend new-node-id station-nodes)
            (vector-push-extend n1 nodes-to-exclude)
            (vector-push-extend n2 nodes-to-exclude)
            (setf px perp-x)
            (setf py perp-y))))
      station-nodes)))

(defun format-station-nodes (nodes)
  "Format the sequence of station nodes NODES into a string representation. (The reason for this strange format is to enable searching using LIKE)"
  (format nil "~{Q~A~}Q" (coerce nodes 'list)))

(defun parse-station-nodes (nodes-text)
  "Get a sequence of station nodes from a string representation generated by FORMAT-STATION-NODES."
  (let* ((ntext (subseq nodes-text 1 (1- (length nodes-text))))
         (ret (split-sequence:split-sequence #\Q ntext)))
    (mapcar #'parse-integer ret)))

(defun link-station-nodes (nodes)
  "Inserts links between the station nodes in NODES."
  (insert-osm-way-links nodes +station-link-tag+ nil))

(defun make-astation (px py)
  "Make a new astation object by running the station identification algorithm starting at (px, py). Returns the ID of the new object created."
  (declare (double-float px py))
  (with-prepared-statements
      ((insert-stmt "INSERT INTO astations (node_members, easting, northing) VALUES (?, ?, ?)"))
    (let* ((station-nodes (identify-station px py))
           (string-form (format-station-nodes station-nodes)))
      (unless (> (length station-nodes) 0)
        (return-from make-astation nil))
      (link-station-nodes station-nodes)
      (bind-parameters insert-stmt string-form px py)
      (sqlite:step-statement insert-stmt)
      (sqlite:last-insert-rowid *db*))))

(defun get-astation-with-node (nid)
  "Get the astation ID of the astation containg the node with ID NID, or NIL if no such astation exists."
  (declare (fixnum nid))
  (with-prepared-statements
      ((get-stmt "SELECT id FROM astations WHERE node_members LIKE ?"))
    (let ((search-string (format nil "Q~AQ" nid)))
      (bind-parameters get-stmt search-string))
    (when (sqlite:step-statement get-stmt)
      (sqlite:statement-column-value get-stmt 0))))

(defun get-astation-node-members (aid)
  "Get a sequence of station nodes for the astation with ID AID, or NIL if no such station exists."
  (declare (fixnum aid))
  (with-prepared-statements
      ((get-stmt "SELECT node_members FROM astations WHERE id = ?"))
    (bind-parameters get-stmt aid)
    (when (sqlite:step-statement get-stmt)
      (with-bound-columns (members) get-stmt
        (parse-station-nodes members)))))

(defun get-astation-tiplocs (aid)
  "Get a sequence of tiplocs for the astation with ID AID, or NIL if no such tiplocs or astation exist(s)."
  (declare (fixnum aid))
  (with-prepared-statements
      ((get-stmt "SELECT tiploc FROM astations_tiploc WHERE station_id = ?"))
    (bind-parameters get-stmt aid)
    (loop
      while (sqlite:step-statement get-stmt)
      collect (sqlite:statement-column-value get-stmt 0))))

(defun get-astation-for-tiploc (tpl)
  "Get an astation ID for the tiploc TPL, if there is one."
  (declare (string tpl))
  (with-prepared-statements
      ((get-stmt "SELECT station_id FROM astations_tiploc WHERE tiploc = ?"))
    (bind-parameters get-stmt tpl)
    (when (sqlite:step-statement get-stmt)
      (with-bound-columns (sid) get-stmt
        sid))))

(defun insert-astation-tiploc (aid tpl)
  "Associate the tiploc TPL with the astation ID AID."
  (declare (string tpl) (fixnum aid))
  (with-prepared-statements
      ((insert-stmt "INSERT INTO astations_tiploc (station_id, tiploc) VALUES (?, ?)"))
    (bind-parameters insert-stmt aid tpl)
    (sqlite:step-statement insert-stmt)))

(defun get-astation-for-location (px py)
  "Get an astation ID for the starting location (px, py), if there is one."
  (declare (double-float px py))
  (with-prepared-statements
      ((get-stmt "SELECT id FROM astations WHERE easting = ? AND northing = ?"))
    (bind-parameters get-stmt px py)
    (when (sqlite:step-statement get-stmt)
      (with-bound-columns (sid) get-stmt
        sid))))

(defun maybe-make-astation-with-tiploc (tpl px py)
  "If no astation exists with the TIPLOC tpl, create one by using the station finding algorithm starting at (px, py). If the algorithm has already been run for that starting location, just add the TIPLOC to the list of that astation's tiplocs. Returns the astation ID."
  (declare (string tpl) (double-float px py))
  (format *debug-io* ";; Making a new astation for TIPLOC ~A at (~,2F, ~,2F)~%" tpl px py)
  (block nil
    (let ((already-tiploc (get-astation-for-tiploc tpl))
          (already-point (get-astation-for-location px py)))
      (when already-tiploc
        (format *debug-io* ";; TIPLOC already exists, aborting~%")
        (return already-tiploc))
      (when already-point
        (format *debug-io* ";; Astation already exists, adding TIPLOC~%")
        (insert-astation-tiploc already-point tpl)
        (return already-point))
      (sqlite:with-transaction *db*
        (let ((aid (make-astation px py)))
          (unless aid
            (format *error-output* ";; TIPLOC ~A is troublesome~%" tpl)
            (push tpl *troublesome-tiplocs*)
            (return))
          (insert-astation-tiploc aid tpl)
          (format *debug-io* ";; Inserted #~A~%" aid)
          aid)))))

(defun make-astations-from-csv-file (file-path)
  "Read 'TIPLOC,EASTING,NORTHING' CSV data from FILE-PATH and attempt to make astation objects with it."
  (with-open-file (file file-path
                        :direction :input)
    (loop
      with line
      while (setf line (read-line file nil nil))
      do (destructuring-bind (tiploc easting northing)
             (split-sequence:split-sequence #\, line)
           (maybe-make-astation-with-tiploc tiploc
                                            (coerce (parse-integer easting) 'double-float)
                                            (coerce (parse-integer northing) 'double-float))))))

(defun get-closest-link (px py &key exclude-nodes (link-flags +rail-tag+))
  "Finds the link closest to the point (px, py)."
  (declare (optimize (speed 3)) (double-float px py) (type (or null (vector fixnum)) exclude-nodes))
  (let ((min-x (- px 3000.0d0))
        (min-y (- py 3000.0d0))
        (max-x (+ px 3000.0d0))
        (max-y (+ py 3000.0d0))
        (greatest-n1 0)
        (greatest-n2 0)
        (perp-x 0.0d0)
        (perp-y 0.0d0)
        (greatest-distance most-positive-double-float))
    (declare (double-float greatest-distance perp-x perp-y) (fixnum greatest-n1 greatest-n2))
    (with-prepared-statement
        (stmt "SELECT n1.osm_id, n1.easting, n1.northing, n2.osm_id, n2.easting, n2.northing FROM links, nodes AS n1, nodes AS n2 WHERE links.p1 = n1.osm_id AND links.p2 = n2.osm_id AND n1.easting BETWEEN ? AND ? AND n1.northing BETWEEN ? and ? AND (links.flags | ?5) > 0")
        (bind-parameters stmt min-x max-x min-y max-y link-flags)
      (loop
        while (sqlite:step-statement stmt)
        do (with-bound-columns (n1-id x1 y1 n2-id x2 y2) stmt
             (declare (type double-float x1 y1 x2 y2)
                      (type fixnum n1-id n2-id))
             (unless (and exclude-nodes (or (find n1-id exclude-nodes)
                                            (find n2-id exclude-nodes)))
               (multiple-value-bind (x3 y3)
                   (line-project-point px py x1 y1 x2 y2)
                 (when x3
                   (let ((dist (the double-float
                                    (squared-pythagoras-distance px py x3 y3))))
                     (when (< dist greatest-distance)
                       (setf greatest-distance dist)
                       (setf greatest-n1 n1-id)
                       (setf greatest-n2 n2-id)
                       (setf perp-x x3)
                       (setf perp-y y3)))))))))
    (when (> greatest-n1 0)
      (values (sqrt greatest-distance) greatest-n1 greatest-n2 perp-x perp-y))))


(defun iterate-all-links ()
  (with-prepared-statement
      (stmt "SELECT * FROM nodes, links WHERE links.p1 = nodes.osm_id OR links.p2 = nodes.osm_id")
      (loop while (sqlite:step-statement stmt))))

(defun get-longest-link ()
  (let ((ret nil))
    (with-prepared-statement
        (stmt "SELECT n1.easting, n1.northing, n2.easting, n2.northing FROM nodes AS n1, nodes AS n2, links WHERE n1.osm_id = links.p1 AND n2.osm_id = links.p2")
        (loop
          while (sqlite:step-statement stmt)
          do (unless ret
               (setf ret 0.0d0))
          do (with-bound-columns (x1 y1 x2 y2) stmt
               (let ((dist (pythagoras-distance x1 y1 x2 y2)))
                 (when (> dist ret)
                   (setf ret dist))))))
    ret))

(defparameter +probe-type-track+ 0)
(defparameter +probe-type-link+ 1)
(defvar *last-probe-id* 0)

(defclass track-probe ()
  ((id
    :initform (incf *last-probe-id*)
    :reader probe-id)
   (desc
    :initarg :desc
    :initform ""
    :accessor probe-desc)
   (nodes-encountered
    :initform (make-array 16
                          :element-type 'fixnum
                          :adjustable t
                          :fill-pointer 0)
    :accessor probe-nodes)
   (tip-node
    :initarg :tip
    :accessor probe-tip)
   (distance
    :initform 0.0d0
    :initarg :distance
    :accessor probe-distance)
   (stations-encountered
    :initform (make-array 16
                          :element-type 'fixnum
                          :adjustable t
                          :fill-pointer 0)
    :accessor probe-stations)
   (split-from
    :initarg :split-from
    :initform nil
    :accessor probe-split-from)
   (ends-up-at
    :initarg :ends-up-at
    :initform nil
    :accessor probe-ends-up-at)
   (exploring
    :initform t
    :accessor probe-exploring)
   (type
    :initarg :type
    :initform +probe-type-track+
    :accessor probe-type)))

(defmethod print-object ((obj track-probe) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (id desc type tip-node distance nodes-encountered stations-encountered exploring ends-up-at) obj
      (format stream "~A~A~A \"~A\" (#~A) dist ~,2F tip ~A #n ~A #s ~A"
              (if (eql type +probe-type-track+) "TRACK" "LINK")
              (if exploring "*" "")
              (if (eql ends-up-at :spur) " SPUR" "")
              desc id distance tip-node
              (length nodes-encountered) (length stations-encountered)))))

(defvar *probes-by-id*)
(defvar *probes-by-nodes*)
(defvar *probes-by-split-from*)
(defvar *global-distance*)
(defvar *exploring-probes*)
(defvar *visited-nodes*)

(defun register-probe-exploring (probe)
  (setf (probe-exploring probe) t)
  (vector-push-extend (probe-id probe) *exploring-probes*))

(defun register-probe-not-exploring (probe)
  (setf (probe-exploring probe) nil)
  (setf *exploring-probes* (delete (probe-id probe) *exploring-probes*)))

(defun register-probe (probe)
  (setf (gethash (probe-id probe) *probes-by-id*) probe)
  (setf (gethash (probe-tip probe) *probes-by-nodes*) probe)
  (register-probe-exploring probe))

(defun make-probe-for-starting-node (nid)
  (let ((ret (make-instance 'track-probe
                            :tip nid)))
    (register-probe ret)
    ret))

(defun make-link-probe (from-probe)
  (let ((ret
          (make-instance 'track-probe
                         :tip (probe-tip from-probe)
                         :distance (probe-distance from-probe)
                         :desc (format nil "~A/s~A"
                                       (probe-desc from-probe)
                                       (probe-id from-probe))
                         :split-from (probe-id from-probe)
                         :type (if (eql (probe-distance from-probe) 0.0d0)
                                   +probe-type-track+
                                   +probe-type-link+))))
    (symbol-macrolet
        ((bsf-entry (gethash (probe-id from-probe) *probes-by-split-from*)))
      (unless bsf-entry
        (setf bsf-entry (make-array 16
                                    :element-type 'fixnum
                                    :fill-pointer 0
                                    :adjustable t)))
      (vector-push-extend (probe-id ret) bsf-entry))
    (register-probe ret)
    ret))

(defun in-place-concatenate (vector new-elements)
  "Adds NEW-ELEMENTS (another vector) to the end of VECTOR."
  (loop
    for elem across new-elements
    for i from 0 below (length new-elements)
    do (vector-push-extend elem vector
                           (- (length new-elements) i))))

(defun merge-probes (parent-probe child-probe)
  "Merge PARENT-PROBE with CHILD-PROBE (the latter being a probe split off from PARENT-PROBE), setting the parent to exploring and deactivating the child."
  (unless (> (length (probe-nodes parent-probe)) 0)
    (format t "; Merging probe ~A with child ~A~%"
            parent-probe child-probe))
  (assert (eql (probe-type child-probe) +probe-type-link+) ()
          "JOIN-PROBES called on non-child probe ~A" (probe-id child-probe))
  (remhash (probe-split-from parent-probe) *probes-by-split-from*)
  (register-probe-not-exploring child-probe)
  (register-probe-exploring parent-probe)
  (setf (probe-distance parent-probe) (probe-distance child-probe))
  (setf (probe-tip parent-probe) (probe-tip child-probe))
  (setf (probe-desc parent-probe) (concatenate 'string
                                               (probe-desc child-probe)
                                               "/m"))
  (in-place-concatenate (probe-nodes parent-probe) (probe-nodes child-probe))
  (in-place-concatenate (probe-stations parent-probe) (probe-stations child-probe)))

(defun handle-probe-termination (probe &optional other-probe-id)
  "Handle the probe PROBE reaching an end, either by hitting into another probe with ID OTHER-PROBE-ID or just running out of track (when OTHER-PROBE-ID is NIL)."
  (format t "; Probe ~A terminated at ~A~%"
          probe other-probe-id)
  (when (eql other-probe-id :spur)
    (remhash (probe-tip probe) *visited-nodes*))
  (register-probe-not-exploring probe)
  (setf (probe-ends-up-at probe) other-probe-id)
  (when (and (eql (probe-type probe) +probe-type-link+)
             (probe-split-from probe))
    (let* ((bsf-entry (gethash (probe-split-from probe) *probes-by-split-from*))
           (other-exploring-probes
             (remove-if (lambda (pid)
                          (let ((prb (gethash pid *probes-by-id*)))
                            (not (and (probe-exploring prb)
                                      (eql (probe-type prb) +probe-type-link+)))))
                        bsf-entry)))
      (when (eql (length other-exploring-probes) 1)
        (let ((parent-probe (gethash (probe-split-from probe) *probes-by-id*))
              (child-probe (gethash (elt other-exploring-probes 0) *probes-by-id*)))
          (merge-probes parent-probe child-probe))))))

(defun consecutive-p (n1 n2 n3)
  "Returns T if N1, N2 and N3 are linked together in that exact order."
  (with-prepared-statements
      ((get-stmt "SELECT p2 FROM links WHERE p1 = ? AND p2 = ?"))
    (bind-parameters get-stmt n1 n2)
    (when (sqlite:step-statement get-stmt)
      (sqlite:reset-statement get-stmt)
      (bind-parameters get-stmt n2 n3)
      (sqlite:step-statement get-stmt))))

(defun in-a-line-p (n1 n2 n3)
  "Returns T if N1, N2 and N3 are linked together, either starting at N1 or N3."
  (or (consecutive-p n1 n2 n3)
      (consecutive-p n3 n2 n1)))

(defun track-following-algorithm (astation-start)
  (let ((starting-nodes (get-astation-node-members astation-start))
        (starting-station-tiplocs (get-astation-tiplocs astation-start))
        (*probes-by-id* (make-hash-table))
        (*probes-by-nodes* (make-hash-table))
        (*probes-by-split-from* (make-hash-table))
        (*visited-nodes* (make-hash-table))
        (*global-distance* 0.0d0)
        (*exploring-probes* (make-array 16
                                      :element-type 'fixnum
                                      :fill-pointer 0
                                      :adjustable t)))
    (setf *last-probe-id* 0)
    (assert starting-nodes () "invalid astation id ~A" astation-start)
    (format t ";; Starting the Track Following Algorithm at ~A with ~A probes~%"
            starting-station-tiplocs (length starting-nodes))
    (map nil #'make-probe-for-starting-node starting-nodes)
    (loop
      for pid across *exploring-probes*
      with probe
      do (setf probe (gethash pid *probes-by-id*))
      do (setf (probe-desc probe) (format nil "start/~A"
                                          (when starting-station-tiplocs
                                            (elt starting-station-tiplocs 0)))))
    (loop
      while (> (length *exploring-probes*) 0)
      do
      (loop
      for pid across (copy-seq *exploring-probes*) ; to avoid iterator invalidation
      do (loop
           named parent-probe-loop
           with probe
           do (setf probe (gethash pid *probes-by-id*))
           while (and (>= *global-distance* (probe-distance probe)) (probe-exploring probe))
           do (let* ((adjnodes
                       (adjacent-nodes (probe-tip probe) +rail-tag+
                                       (when (> (length (probe-nodes probe)) 0)
                                         (elt (probe-nodes probe)
                                              (1- (length (probe-nodes probe)))))))
                     (many-adjnodes-p (> (length adjnodes) 1)))
                (format t "~A for ~A~%" adjnodes probe)
                (when (or
                       (eql (length adjnodes) 0)
                       (every (lambda (x)
                                (gethash x *visited-nodes*))
                              adjnodes)
                       (and
                        ;; This link is a spur (there's a straight line running from one
                        ;; node to another, with a junction inbetween where this link joins
                        ;; on).
                        ;; In this case, terminate the link at the junction point, so it
                        ;; doesn't try and explore up the main route and break stuff.
                        (eql (probe-type probe) +probe-type-link+)
                        (eql (length adjnodes) 2)
                        (in-a-line-p (elt adjnodes 0) (probe-tip probe) (elt adjnodes 1))))
                  (handle-probe-termination probe
                                            (unless (eql (length adjnodes) 0)
                                              :spur))
                  (return-from parent-probe-loop))
                (when many-adjnodes-p
                  (register-probe-not-exploring probe))
                (loop
                  named new-probe-loop
                  for adjnode in adjnodes
                  with new-probe
                  when (gethash adjnode *visited-nodes*)
                    do (return-from new-probe-loop)
                  do (setf new-probe (if many-adjnodes-p
                                         (make-link-probe probe)
                                         probe))
                  do (incf (probe-distance new-probe)
                           (distance-between-nodes (probe-tip new-probe) adjnode))
                  do (vector-push-extend adjnode (probe-nodes new-probe))
                  do (setf (gethash adjnode *visited-nodes*) t)
                  when (> (probe-distance new-probe) *global-distance*)
                    do (progn
                         (format t ";; New distance ~A by ~A~%" (probe-distance new-probe) new-probe)
                         (setf *global-distance* (probe-distance new-probe)))
                  do (setf (probe-tip new-probe) adjnode)
                  when (node-contains-flag-p adjnode +station-node-tag+)
                    do (let* ((aid (get-astation-with-node adjnode))
                              (tiplocs (when aid
                                         (get-astation-tiplocs aid)))
                              (name-to-use (if (> (length tiplocs) 0)
                                               (elt tiplocs 0)
                                               (format nil "#~A" aid))))
                         (when aid
                           (setf (probe-type new-probe) +probe-type-track+)
                           (setf (probe-desc new-probe) (concatenate 'string
                                                                     (probe-desc new-probe)
                                                                     "/" name-to-use))
                           (format t "; Probe ~A encountered astation #~A (tpls ~A)~%"
                                   new-probe aid tiplocs)
                           (vector-push-extend adjnode (probe-stations new-probe))))
                  with joinable-probe = (gethash adjnode *probes-by-nodes*)
                  when joinable-probe
                    do (if (eql (probe-ends-up-at joinable-probe) :spur)
                           (handle-probe-termination joinable-probe new-probe)
                           (progn
                             (handle-probe-termination new-probe joinable-probe)
                             (return-from new-probe-loop)))
                  do (setf (gethash adjnode *probes-by-nodes*) new-probe))))))
    (format t ";; Track Finding Algorithm terminated~%")
    (loop
      for probe being the hash-values in *probes-by-id*
      when (eql (probe-type probe) +probe-type-track+)
        collect probe)))

(defun insert-osm-way (way)
  "Inserts the OSM way WAY into the database by making links between nodes."
    (let* ((refs (osmpbf:way-refs way))
           (tags (osmpbf:osm-obj-tags way))
           (flags (get-flags-for-tags tags))
           (parent-way-id (osmpbf:osm-obj-id way)))
      (insert-osm-way-links refs flags (osmpbf:osm-obj-id way))
      (incf *ways-inserted*)
      (when (contains-flag-p flags +station-tag+)
        (insert-osm-station nil parent-way-id tags))))

(defun insert-primitive-group (pg)
  "Inserts the PBF primitive group PG into the database, by inserting its nodes and ways (if any)."
  (mapc #'insert-osm-node (osmpbf:group-nodes pg))
  (mapc #'insert-osm-way (osmpbf:group-ways pg)))

(defun insert-from-pbf (file)
  "Inserts all the things from the PBF file FILE into the database. Probably takes a while."
  (setf *nodes-inserted* 0)
  (setf *ways-inserted* 0)
  (unless (ostn15-available-p)
    (error "The OSTN15 National Grid data file is not installed. Please refer to projection.md for instructions on installation."))
  (setf *wgs84-to-grid-obj* (get-osgb-transformation-object))
  (unwind-protect
       (progn
         (sqlite:execute-non-query *db* "PRAGMA synchronous = OFF")
         (sqlite:execute-non-query *db* "PRAGMA journal_mode = MEMORY")
         (sqlite:with-transaction *db*
           (loop with groups = nil
                 while (setf groups (osmpbf:get-next-primitive-groups file
                                                                      :eof-error-p nil))
                 do (progn
                      (mapc #'insert-primitive-group groups)
                      (format *debug-io* "; ~A nodes / ~A ways / ~A stations~%"
                              *nodes-inserted* *ways-inserted* *stations-inserted*)))))
    (run-pragmas)))

(defun insert-from-pbf-filepath (path)
  "Inserts all the things from the PBF file at PATH into the database. Probably takes a while."
  (with-open-file (file path
                        :direction :input
                        :element-type '(unsigned-byte 8))
    (insert-from-pbf file)))
