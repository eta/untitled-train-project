(defsystem "utp"
  :depends-on ("sqlite" "osmpbf" "cl-heap" "cffi" "cffi-libffi" "split-sequence")
  :serial t
  :components
  ((:file "packages")
   (:file "haversine")
   (:file "proj")
   (:file "mapping")))
