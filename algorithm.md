# The Track Following Algorithm

*an attempt at solving the mapping side of trainsplorer, for good*

## Common algorithms

- *Finding the heading* of a piece of track T with nodes N1 and N2 means finding the angle between a point due North of N1, N1, and N2 (i.e. the bearing of N2 from N1), and returning this value modulo 180.

## Station identification

Given a point S that's nearby a set of station tracks:

- Initialise an array X which is to contain a set of station points.
- Find the piece of track that's nearest S, T1. Flag this piece of track as part of the station.
- Drop a perpendicular P from S to T1.
- Loop, starting with Tn = T1:
  - *Find the heading* of Tn, comparing it to the last observed heading H (skip the comparison if H is null):
    - If new H and old H differ by a constant number of degrees (say, 10°), abort iteration.
    - Otherwise, continue, setting H to the found heading.
  - Split Tn at the point where it intersects P, making a new point Q. Add Q to X.
  - Find the piece of track that's nearest Q, Tn+1 (and isn't already flagged as part of the station).
    - Cap the search at a reasonable constant value (e.g. 15 metres). If no track is found within this range, abort.
  - Drop a new perpendicular P from Q to Tn+1, set Tn to Tn+1, and loop.
- Create a new 'station' object containing all the elements of X, and persist it in the database.

## Track following

Define a 'probe' object with the following attributes:
- set of links
- current tip link & node
- distance travelled
- stations encountered (node ref, station ref)
- UUID
- split from: a parent probe object, or null
- ends up at: a parent probe object, or null
- exploring: true
- type (either 'track' or 'link')

Given a station object containing points X:
- Make two sets of probe objects, 'up' and 'down'.
- For each point Xn in X, get all links connected to Xn.
  - If there aren't exactly 2, abort. (We assume that each Xn has precisely 2 links due to how it was created in 'Station identification' above.)
  - *Find the heading* of each link **without** doing the modulo 180. Add the link with a heading > 180 to 'up', and the other one to 'down'.
    - More precisely, make a probe object with the attributes:
      - set of links: the one link
      - current tip link & node: this one link, and the node that isn't Xn
      - distance travelled: the Haversine length of this link
      - stations encountered: the current station object & node Xn
      - type: 'track'
      - UUID: randomly generate
    - ...and add that.
    - (If two or none have a heading > 180, abort.)
- Set the current list of probes P to 'up' (and repeat this algorithm again once terminated, setting it to 'down' for the second iteration).
- Set the global distance travelled to the maximum distance travelled value in P.
- For each probe in the list of probes which have exploring set to true:
  - while the global distance travelled is strictly greater than this probe's distance travelled, and this probe is still marked as exploring:
    - Find the links L connected to the current tip node (that aren't the tip link).
    - For each link Ln in L:
      - if the length of L is 1, apply the following instructions to the current probe object
        - if not, create a new probe object on each iteration of type 'link', with 'split from' set to the current probe object...
        - ...and set the current probe object's 'exploring' value to false
      - increment the distance travelled by the Haversine length of the link
      - set the global distance travelled to the distance travelled, if it is greater
      - set the current tip link to this one link, and the tip node to the node at the other end
      - if the tip node is part of a station:
        - set 'type' to 'track'
        - add the station object to 'stations encountered'
      - if the tip node is a member of any of the links in any of the other probe objects:
        - get the singular probe object this applies to; if there's more than one, abort
        - set 'exploring' to false
        - set 'ends up at' to this probe object
        - get the list of probe objects O that share the same 'split from' value as this one, and have exploring set to true
          - if 'split from' is null, exit out of this if
        - if O has one element only:
          - merge this probe object with its parent ('split from'), and set its parent's 'exploring' value to true
      - if a new probe object was created, add it to the list of probes (at the end)
    - if there are no links in L:
      - set 'exploring' to false
      - set 'ends up at' to null
      - get the list of probe objects O that share the same 'split from' value as this one, and have exploring set to true
        - if 'split from' is null, exit out of this if
      - if O has one element only:
        - merge this probe object with its parent ('split from'), and set its parent's 'exploring' value to true
