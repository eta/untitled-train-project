;;;; Doing geometric projections with the PROJ library and CFFI

(in-package :railway-slurper)

(cffi:define-foreign-library libproj
  (t (:default "libproj")))

(cffi:use-foreign-library libproj)

(cffi:defctype pj :pointer)
(cffi:defctype pj-context :pointer)
(cffi:defcunion pj-coord
  (coords :double :count 4))
(cffi:defcstruct pj-lp
  (lam :double)
  (phi :double))
(cffi:defcstruct pj-grid-info
  (gridname :char :count 32)
  (filename :char :count 260)
  (format :char :count 8)
  (lowerleft (:struct pj-lp))
  (upperright (:struct pj-lp))
  (n-lon :int)
  (n-lat :int)
  (cs-lon :double)
  (cs-lat :double))


(cffi:defcfun proj-context-create (:pointer pj-context))
(cffi:defcfun proj-create (:pointer pj)
  (ctx (:pointer pj-context))
  (definition (:pointer :char)))
(cffi:defcfun proj-create-crs-to-crs (:pointer pj)
  (ctx (:pointer pj-context))
  (source-crs (:pointer :char))
  (target-crs (:pointer :char))
  (area :pointer))
(cffi:defcfun proj-trans (:union pj-coord)
  (pj (:pointer pj))
  (direction :int)
  (coord (:union pj-coord)))
(cffi:defcfun proj-grid-info (:struct pj-grid-info)
  (gridname (:pointer :char)))

(defparameter +pj-fwd+ (the fixnum 1))
(defparameter +pj-inv+ (the fixnum -1))
(defparameter +wgs84-crs+ "EPSG:4236")
(defparameter +national-grid-crs+ "EPSG:27700")
(defparameter +conversion-string+ "+proj=pipeline +step +proj=axisswap +order=2,1 +step +proj=unitconvert +xy_in=deg +xy_out=rad +step +inv +proj=hgridshift +grids=OSTN15_NTv2_OSGBtoETRS.gsb +step +proj=tmerc +lat_0=49 +lon_0=-2 +k=0.9996012717 +x_0=400000 +y_0=-100000 +ellps=airy")
(defparameter +ostn-grid-name+ "OSTN15_NTv2_OSGBtoETRS.gsb")

(defvar *pj-ctx* nil)
(defvar *pj-crs-objects* (make-hash-table
                          :test #'equal))

(defun get-pj-ctx ()
  (if *pj-ctx*
      *pj-ctx*
      (setf *pj-ctx* (proj-context-create))))

(defun get-crs-object (from-crs to-crs)
  (let ((hash-key (cons from-crs to-crs)))
    (symbol-macrolet
        ((crs-obj (gethash hash-key *pj-crs-objects*)))
      (if crs-obj
          crs-obj
          (let* ((pj-ctx (get-pj-ctx))
                 (new-obj
                   (cffi:with-foreign-strings
                       ((from from-crs)
                        (to to-crs))
                     (proj-create-crs-to-crs pj-ctx from to (cffi:null-pointer)))))
            (when (cffi:null-pointer-p new-obj)
              (error "Couldn't make new CRS to CRS object for ~A to ~A" from-crs to-crs))
            (setf crs-obj new-obj))))))

(defun get-osgb-transformation-object ()
  (let* ((pj-ctx (get-pj-ctx))
         (new-obj
           (cffi:with-foreign-strings
               ((convstr +conversion-string+))
             (proj-create pj-ctx convstr))))
    (when (cffi:null-pointer-p new-obj)
      (error "Couldn't get OSGB transformation object"))
    new-obj))

(defun get-grid-format (grid-name)
  (cffi:with-foreign-string (gn grid-name)
    (let* ((grid-info (proj-grid-info gn))
           (format (getf grid-info 'format)))
        (cffi:foreign-string-to-lisp format))))

(defun ostn15-available-p ()
  (string= (get-grid-format +ostn-grid-name+) "ntv2"))

(defmacro with-pj-coord ((coord x y z &key reverse) &body body)
  `(cffi:with-foreign-object (,coord '(:union pj-coord))
     (cffi:with-foreign-slots ((coords) ,coord (:union pj-coord))
       (setf (cffi:mem-aref coords :double 0) ,x)
       (setf (cffi:mem-aref coords :double 1) ,y)
       (setf (cffi:mem-aref coords :double 2) ,z)
       ,@(if reverse
             (let ((ret (gensym)))
               `((let ((,ret (progn ,@body)))
                  (values
                   (cffi:mem-aref ,ret :double 0)
                   (cffi:mem-aref ,ret :double 1)
                   (cffi:mem-aref ,ret :double 2)))))
             body))))

(defun proj-translate (crs-obj x y z &optional (direction +pj-fwd+))
  (declare (double-float x y z))
  (with-pj-coord (coord x y z :reverse t)
    (proj-trans crs-obj direction coord)))
