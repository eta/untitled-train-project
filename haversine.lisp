;; Calculating distances using the Haversine formula

(in-package :railway-slurper)

(declaim (inline to-radians))
(defun to-radians (degrees)
  "Convert DEGREES to radians."
  (declare (double-float degrees))
  (* pi (/ degrees 180)))

(defparameter +mean-earth-radius+ 6371000.0d0)

(defmacro with-radians-from-degrees (degrees &body forms)
  "Convert the variable names in DEGREES from degrees to radians, and run FORMS with them rebound to their radian values.."
  (let ((let-forms (loop
                     for ident in degrees
                     collect `(,ident (to-radians ,ident)))))
    `(let ,let-forms ,@forms)))

(defun haversine-distance (lat1 lon1 lat2 lon2)
  (declare (double-float lat1 lon1 lat2 lon2))
  (with-radians-from-degrees (lat1 lon1 lat2 lon2)
    (let* ((delta-lon (- lon2 lon1))
           (delta-lat (- lat2 lat1)))
      (* (* 2.0d0 +mean-earth-radius+)
         (asin (sqrt (the (double-float 0d0) (+
                      (expt (sin (/ delta-lat 2.0d0)) 2.0d0)
                      (* (cos lat1)
                         (cos lat2)
                         (expt (sin (/ delta-lon 2.0d0)) 2.0d0))))))))))


